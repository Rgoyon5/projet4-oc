# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import ChromeOptions

class Test_checkout():
  def setup_method(self, method):

    #configure chrome in headless to work in containers
    options = ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-extensions')
    options.add_argument("--no-sandbox")
    self.driver = webdriver.Remote('http://localhost:4444/wd/hub', options.to_capabilities())


    self.driver.get("http://localhost:8000/cart/shop/nshirt/")
    self.driver.set_window_size(1608, 1023)
    self.driver.find_element(By.ID, "id_quantity").click()
    self.driver.find_element(By.ID, "id_quantity").send_keys("1")
    self.driver.find_element(By.ID, "id_colour").click()
    dropdown = self.driver.find_element(By.ID, "id_colour")
    dropdown.find_element(By.XPATH, "//option[. = 'green']").click()
    self.driver.find_element(By.CSS_SELECTOR, "#id_colour > option:nth-child(2)").click()
    self.driver.find_element(By.ID, "id_size").click()
    dropdown = self.driver.find_element(By.ID, "id_size")
    dropdown.find_element(By.XPATH, "//option[. = 's']").click()
    self.driver.find_element(By.CSS_SELECTOR, "#id_size > option:nth-child(2)").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
  
  def teardown_method(self, method):
    self.driver.quit()
  

    
  def test_is_item_size_s(self):
    pass

  def test_is_item_colour_green(self):
    pass

